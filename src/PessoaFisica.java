import java.util.Date;

public class PessoaFisica extends Pessoa {
    public String cpf;

    public PessoaFisica(String _nome, String _cpf){
        super(_nome);
        this.cpf = _cpf;
    }
}