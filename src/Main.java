import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Aplicativo aplicativo = new Aplicativo();
        aplicativo.criarCarteira();
        Boolean continuar = Boolean.TRUE;
        Scanner entrada = new Scanner(System.in);
        String aux = "";
        while (continuar==Boolean.TRUE){
            System.out.println("O que voce quer fazer? Abrir conta PF: (1) Abrir conta PJ: (2) Imprimir Carteira PF: (3) Imprimir Carteira PJ: (4)");
            aux = entrada.nextLine();
            if(aux.equals("1")){
                String nome, cpf;
                System.out.println("Nome: ");
                nome = entrada.nextLine();
                System.out.println("CPF: ");
                cpf = entrada.nextLine();
                PessoaFisica pf = new PessoaFisica(nome, cpf);
                aplicativo.abrirContaPF(pf);
            } else if(aux.equals("2")){
                String nome, cnpj, nomeFantasia;
                System.out.println("Nome: ");
                nome = entrada.nextLine();
                System.out.println("CNPJ: ");
                cnpj = entrada.nextLine();
                System.out.println("Nome Fantasia: ");
                nomeFantasia = entrada.nextLine();
                PessoaJurifica pj = new PessoaJurifica(nome, cnpj, nomeFantasia);
                aplicativo.abrirContaPJ(pj);
            } else if(aux.equals("3")){
                aplicativo.imprimirCarteiraPF();
            } else if(aux.equals("4")){
                aplicativo.imprimirCarteiraPJ();
            } else{
                System.out.println("Valor Inv�lido\n");
            }
            System.out.println("Deseja Fazer outra opera��o? S/N");
            if(entrada.nextLine().equals("S")){
                continuar = Boolean.TRUE;
            } else {
                continuar = Boolean.FALSE;
            }
        }
        System.out.println("Obrigado!");
    }
}
