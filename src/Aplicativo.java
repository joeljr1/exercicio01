import java.util.ArrayList;
import java.util.List;

public class Aplicativo<carteira> extends CarteiraCliente {
    //private List<PessoaFisica> carteiraPF;
    //private List<PessoaJurifica> carteiraPJ;

    public Aplicativo(){
        super();
    }

    public void criarCarteira(){
        this.carteiraPF = new ArrayList<PessoaFisica>();
        this.carteiraPJ = new ArrayList<PessoaJurifica>();
    }

    public void abrirContaPJ(PessoaJurifica pj){
        this.carteiraPJ.add(pj);
    }

    public void abrirContaPF(PessoaFisica pf){
        this.carteiraPF.add(pf);
    }

    public void imprimirCarteiraPF(){
        for (int i = 0; i < this.carteiraPF.size(); i++) {
            System.out.println("Cliente: "+Integer.toString(i));
            System.out.println("Nome:"+this.carteiraPF.get(i).nome);
            System.out.println("CPF:"+this.carteiraPF.get(i).cpf);
        }
    }

    public void imprimirCarteiraPJ(){
        for (int i = 0; i < this.carteiraPJ.size(); i++) {
            System.out.println("Cliente: "+Integer.toString(i));
            System.out.println("Nome:"+this.carteiraPJ.get(i).nome);
            System.out.println("CNPJ:"+this.carteiraPJ.get(i).cnpj);
            System.out.println("Nome Fantasia:"+this.carteiraPJ.get(i).nomeFantasia);
        }
    }
}
